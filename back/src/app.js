// tutorial produzido por balta
// https://github.com/balta-io/1972/
// construção por josé mário, 01/02/2020

"use strict"

const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const config = require("./config")

const app = express()
const router = express.Router()
0
// Conecta ao banco
mongoose.connect(config.connectionString)
//mongoose.set("useFindAndModify", false)

// Carrega models
const Post = require("./models/post-model")

// Carrega as rotas
const postRoute = require("./routes/post-route")
//const userRoute = require("./routes/user-route")

app.use(
  bodyParser.json({
    limit: "5mb"
  })
)
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)

// Habilita o CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://127.0.0.1:5500")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, x-access-token"
  )
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  )
  next()
})

app.use("/posts", postRoute)
//app.use("/users", userRoute)

module.exports = app
