var url = "http://localhost:3000"

function getAllPosts() {
  var xhr = new XMLHttpRequest()

  xhr.open("GET", url + "/posts")

  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 200) {
      //document.body.innerHTML=
      records = JSON.parse(xhr.responseText)
      retorno = `
        <div class="label"> 
          <span>  Listagem de Posts - rota GET /posts </span>
        </div>
        <ul>`
      for (i = 0; i < records.length; i++) {
        retorno += `
          <li onclick="getPost('${records[i]._id}')">
            ${records[i].title} 
            <div>
              <a onclick="formEditarPost('${records[i]._id}')"><i class="fa fa-edit"></i></a> 
              <a onclick="deletarPost('${records[i]._id}')"><i class="fa fa-times"></i></a>
            </div>
          </li>`
      }
      retorno += `</ul>`

      document.getElementById("mainEsq").innerHTML = retorno
    }
  }
  xhr.send()
}

function getPost(id) {
  var xhr = new XMLHttpRequest()

  xhr.open("GET", `${url}/posts/${id}`)

  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 200) {
      //document.body.innerHTML=
      post = JSON.parse(xhr.responseText)
      retorno = `
        <div class="post">
          <h1> ${post.title} </h1>
          <p> ${post.body} </p>`

      if (post.comments.length != 0) {
        retorno += `
          <p class="label-lista-comments"> Commentários: </p>`
      }
      console.log(post.comments)
      for (i = 0; i < post.comments.length; i++) {
        retorno += `
          <div class="comment"> 
            <h3> ${post.comments[i].name} </h3>
            <span> ${post.comments[i].email} </span>
            <p>${post.comments[i].body}</p>
          </div>`
      }
      retorno += `
          <div class="comment"> 
            <h3> <input id="nameNovoComment" placeholder="escreva seu nome"/> </h3>
            <span> <input id="emailNovoComment" placeholder="escreva seu email"/> </span>
            <p> <input id="bodyNovoComment" placeholder="escreva seu comentário"/> </p>
            <a onclick="novoComment('${post._id}')" id="enviarNovoComment"> Gravar novo comment! </a>
          </div>
        </div>`

      document.getElementById("mainEsq").innerHTML = retorno
    }
  }
  xhr.send()
}

function novoPost() {
  enviarNovoPost = document.getElementById("enviarNovoPost")
  enviarNovoPost.classList.add("desativado")
  enviarNovoPost.innerText = "Enviando..."

  var xhr = new XMLHttpRequest()
  xhr.open("POST", url + "/posts")

  xhr.setRequestHeader("Content-Type", "application/json")
  var dados = {
    title: document.getElementById("titleNewPost").value,
    body: document.getElementById("bodyNewPost").value
  }

  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 201) {
      alert(JSON.parse(xhr.responseText).message)
      document.getElementById("formNovoPost").reset()

      getAllPosts()
    } else if (xhr.readyState == xhr.DONE && xhr.status == 400) {
      console.log(xhr.responseText)
      mensagens = JSON.parse(xhr.responseText)
      resposta = ""
      for (i = 0; i < mensagens.length; i++) {
        resposta += mensagens[i].message
      }
      alert(resposta)
    }
    if (xhr.readyState == xhr.DONE) {
      enviarNovoPost.classList.remove("desativado")
      enviarNovoPost.innerText = "Registrar e atualizar!"
    }
  }
  xhr.send(JSON.stringify(dados))
}

function deletarPost(id) {
  var xhr = new XMLHttpRequest()

  xhr.open("DELETE", `${url}/posts/${id}`)

  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 200) {
      alert(JSON.parse(xhr.responseText).message)
      getAllPosts()
    }
  }
  xhr.send()

  event.stopPropagation()
}

function formEditarPost(id) {
  var xhr = new XMLHttpRequest()
  xhr.open("GET", `${url}/posts/${id}`)
  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 200) {
      post = JSON.parse(xhr.responseText)
      retorno = `
        <form action="javascript:editarPost('${post._id}')" method="post" id="formUpdatePost">
          <input
            type="text"
            placeholder="${post.title}"
            name="title"
            id="titleUpdatePost"
          />
          <textarea
            name="body"
            id="bodyUpdatePost"
            cols="30"
            rows="10"
            placeholder="${post.body}"
          ></textarea>
          <button type="submit" id="enviarUpdatePost">
            Registrar e atualizar!
          </button>
    </form>`

      document.getElementById("mainEsq").innerHTML = retorno
    }
  }
  xhr.send()

  event.stopPropagation()
}

function editarPost(id) {
  enviarUpdatePost = document.getElementById("enviarUpdatePost")
  enviarUpdatePost.classList.add("desativado")
  enviarUpdatePost.innerText = "Enviando..."

  var xhr = new XMLHttpRequest()
  xhr.open("PUT", `${url}/posts/${id}`)

  xhr.setRequestHeader("Content-Type", "application/json")
  var dados = {
    title: document.getElementById("titleUpdatePost").value,
    body: document.getElementById("bodyUpdatePost").value
  }

  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 200) {
      alert(JSON.parse(xhr.responseText).message)
      document.getElementById("formUpdatePost").reset()
      getAllPosts()
    }
  }
  xhr.send(JSON.stringify(dados))
}

function novoComment(id) {
  enviarNovoComment = document.getElementById("enviarNovoComment")
  enviarNovoComment.classList.add("desativado")
  enviarNovoComment.innerText = "Enviando..."

  var xhr = new XMLHttpRequest()
  xhr.open("POST", `${url}/posts/${id}/comments`)

  xhr.setRequestHeader("Content-Type", "application/json")
  var dados = {
    name: document.getElementById("nameNovoComment").value,
    email: document.getElementById("emailNovoComment").value,
    body: document.getElementById("bodyNovoComment").value
  }

  xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 201) {
      alert(JSON.parse(xhr.responseText).message)
      getPost(id)
    } else if (xhr.readyState == xhr.DONE && xhr.status == 400) {
      console.log(xhr.responseText)
      mensagens = JSON.parse(xhr.responseText)
      resposta = ""
      for (i = 0; i < mensagens.length; i++) {
        resposta += mensagens[i].message
      }
      alert(resposta)
    }
    if (xhr.readyState == xhr.DONE) {
      enviarNovoPost.classList.remove("desativado")
      enviarNovoPost.innerText = "Registrar e atualizar!"
    }
  }
  xhr.send(JSON.stringify(dados))
}
