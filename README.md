# Basic Blog

This is a website that allows you to:

- Create, read, update and delete posts
- Add comments in each post

## Why am I still maintaining this repository?

It is an exercise to my knowledge, and a good way to keep track in my advancements throughout the time.

## Technologies used

- Backend is a REST API in node.js
- Front-end in vanilla javascript.
- Mongo Database
- API Tests in INSOMNIA

## Screenshots

Here you can see the main page of the app:
![](https://i.imgur.com/e6vLhXv.png)
and here is the page you get if you click in one of the posts:
![](https://i.imgur.com/YE5sjNB.png)

## Setup:

### Backend:

```
$ cd back

$ npm install

$ nodemon ./bin/server.js
```

Also, for the database to work, the server IP should be whitelisted in Atlas, and after that your own connectionString should be put in config.js

### Frontend:

Launch front/index.html with a live server in the port 5500 (the origin is configured to be http://127.0.0.1:5500 for the server to work), and it is all set!
